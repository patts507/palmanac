var gulp = require('gulp');
var spawn = require('child_process').spawn;
var node;

gulp.task('default', function() {
	var watchList = [
		'./server.js',
		'./public/**',
		'./app/**'
	];
  gulp.run('server');
  gulp.watch(watchList, function() {
    gulp.run('server');
  });
})

gulp.task('server', function() {
  if (node) node.kill();
  node = spawn('node', ['server.js'], {stdio: 'inherit'});
  node.on('close', function (code) {
    if (code === 8) {
      gulp.log('Error detected, waiting for changes...');
    }
  });
});

process.on('exit', function() {
	if (node) node.kill();
});

