'user strict';
module.exports = function (app) {

  app.get('/', function (req,res) {
    res.render('index');
  });

  app.get('/information', palmanacInformation);
  app.post('/calculator', palmanacCalculator);

  //app.post('/instance', middleware.auth, instanceCreate);

};
