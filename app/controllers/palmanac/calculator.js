'use strict';
var validate = require('../../libs/validate');
var regex = require('../../libs/regex');
var helper = require('../../libs/helper');
var calculator = require('../../services/calculator');
var joi = require('joi');
var _ = require('lodash');

module.exports = function(req, res){

  var schema = joi.object().keys({
    appname: joi.string().required(),
    eqxversion: joi.string().valid(config.information.eqxVersions).required(),
    ec: joi.string().valid(config.information.ecs).required(),
    log: joi.number().min(config.information.minsizeKB).max(config.information.maxsizeKB).required(),
    msgsize: joi.number().min(config.information.minsizeKB).max(config.information.maxsizeKB).required(),
    http: joi.number().min(config.information.minProtocol).max(config.information.maxProtocol).required(),
    diameter: joi.number().min(config.information.minProtocol).max(config.information.maxProtocol).required(),
    ldap: joi.number().min(config.information.minProtocol).max(config.information.maxProtocol).required(),
    smpp: joi.number().min(config.information.minProtocol).max(config.information.maxProtocol).required(),
    tcp: joi.number().min(config.information.minProtocol).max(config.information.maxProtocol).required(),
    other: joi.number().min(config.information.minProtocol).max(config.information.maxProtocol).required(),
    weight: joi.number().min(config.information.minWeight).max(config.information.maxWeight).required(),
    concurrent: joi.number().min(config.information.minConcurrent).max(config.information.maxConcurrent).required(),
    reqtran: joi.number().min(config.information.minReqtran).max(config.information.maxReqtran).required(),
    cornumber: joi.number().min(config.information.minCornumRange).max(config.information.maxCornum).required(),
    typeofmach: joi.string().valid(config.information.typeofmachs).required(),
    Protocol: joi.number().min(1).max(config.information.maxProtocol).required()
  });

  var constant = config.calculator,
      _params = undefined,
      _result1 = undefined,
      _result2 = undefined,
      _result3 = undefined,
      _result4 = undefined,
      _result5 = undefined,
      _result6 = undefined,
      data = undefined;

  validate.joi(req.body, schema)
    .then(function(p){
      _params = p;
      return calculator.getAns1(_params.ec, _params.cornumber);
    })
    .then(function(result1){
      _result1 = result1;
      return calculator.getAns2(_result1, _params.eqxversion, _params.msgsize);
    })
    .then(function(result2){
      _result2 = result2;
      return calculator.getAns3(_result2, _params.log, _params.typeofmach);
    })
    .then(function(result3){
      _result3 = result3;
      return calculator.getAns4(_params.http, _params.diameter, _params.ldap, _params.smpp, _params.tcp, _params.other, _params.weight);
    })
    .then(function(result4){
      _result4 = result4;
      return calculator.getAns5(_result3, _result4, _params.concurrent)
    })
    .then(function(result5){
      _result5 = result5;
      return calculator.getAns6(_result5, _params.reqtran)
    })
    .then(function(result6){
      _result6 = result6;
      data = {tps:_result5, rps:_result6};
      return [200, data];
    })
    .then(function(data){
      helper.responseSuccess(res, data);
    })
    .catch(function(err){
      console.log(err);
      helper.responseError(res, err);
    });

};

