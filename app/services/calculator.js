var _ = require('lodash');

module.exports =  {

  getAns1: function(ec, cornumber) {
    if(ec == 'EC00'){
      return ((cornumber - 1) * config.calculator.ans1ec00);
    }
    else if(ec == 'EC02'){
      return ((cornumber -1) * config.calculator.ans1ec02);
    }
    else{
      return '';
    }
  },

  getAns2: function(result1, eqxversion, messagesize){
    if (eqxversion == '1.3.x') {
      if (messagesize > config.calculator.ans2msgsizemorethan) {
        return (result1 / Math.sqrt(messagesize * config.calculator.ans2msgmultiply)) * config.calculator.ans2eqxversion13x;
      }
      else {
        return result1 * config.calculator.ans2eqxversion13x;
      }
    }
    else if (eqxversion == '1.4.x') {
      if (messagesize > config.calculator.ans2msgsizemorethan) {
        return (result1 / Math.sqrt(messagesize * config.calculator.ans2msgmultiply));
      }
      else {
        return result1;
      }
    }
    else if (eqxversion == '1.5.x') {
      if (messagesize > config.calculator.ans2msgsizemorethan) {
        return (result1 / Math.sqrt(messagesize * config.calculator.ansmsgmultiply)) * config.calculator.ans2eqxversion15x;
      }
      else {
        return result1 * config.calculator.ans2eqxversion15x;
      }
    }
  },

  getAns3: function(result2, log, typeofmach){
    if (log > config.calculator.ans3logmorethan) {
      if (typeofmach == 'vm') {
        return (result2 / Math.sqrt(log * config.calculator.ans3logmultiply));
      }
      else if (typeofmach == 'bare') {
        return ((result2 * config.calculator.ans3ans2multiply) / Math.sqrt(log * config.calculator.ans3logmultiply));
      }
    }
    else {
      return result2;
    }
  },

  getAns4: function(protocolhttp, protocoldiameter, protocolldap, protocolsmpp, protocoltcp, protocolother, weight){
  return (((protocolhttp * config.calculator.ans4http) + protocoldiameter + protocolldap +
    (protocolsmpp * config.calculator.ans4smpp) + (protocoltcp * config.calculator.ans4tcp) +
    (protocolother * weight)) * config.calculator.ans4multiply);
  },

  getAns5: function(result3, result4, concurrent){
    return Math.round(((result3 * result4) - Math.sqrt(Math.sqrt(concurrent))));
  },

  getAns6: function(result5, reqtran){
    return Math.round(result5 / reqtran);
  }

};
