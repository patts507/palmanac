var _ = require('lodash');

env = process.env.NODE_ENV || 'development'
process.env.NODE_ENV = env

if(process.env.CIPHER_KEY) {
  _cipherKey = new Buffer(process.env.CIPHER_KEY);
} else {
  _cipherKey = new Buffer('AbcdeFgHiJKLmnOPQRStuVWXYz234519');
}

if(process.env.HMAC_KEY) {
  _hmackey = new Buffer(process.env.HMAC_KEY);
} else {
  _hmackey = new Buffer('supersecret');;
}

config = {
  information:{
    nameExport: 'exportPalmanac.csv',
    ecs: ['EC00', 'EC02'],
    eqxVersions: ['1.3.x', '1.4.x', '1.5.x'],
    typeofmachs: ['bare', 'vm'],
    minsizeKB: 0,
    maxsizeKB: 8192,
    minProtocol: 0,
    maxProtocol: 100,
    minWeight: 0,
    maxWeight: 1,
    minConcurrent: 0,
    maxConcurrent: 1000000,
    minReqtran: 0,
    maxReqtran: 1000000,
    minCornum: 1,
    minCornumRange: 4,
    maxCornum: 80
  },
  calculator:{
    ans1ec00: 3333,
    ans1ec02: 666,
    ans2msgsizemorethan: 0.083,
    ans2msgmultiply: 10,
    ans2eqxversion13x: 0.7,
    ans2eqxversion15x: 1.07,
    ans3logmorethan: 0.33,
    ans3logmultiply: 3,
    ans3ans2multiply: 1.05,
    ans4http: 0.8,
    ans4smpp: 0.9,
    ans4tcp: 0.8,
    ans4multiply: 0.01
  }
}
module.exports = {};
