// Karma configuration
// Generated on Mon Jan 18 2016 09:49:02 GMT+0700 (SE Asia Standard Time)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [
      'public/lib/angular/angular.js',
      'public/lib/angular-mocks/angular-mocks.js',
      'public/lib/angular-ui-router/release/angular-ui-router.js',
      'public/lib/angular-animate/angular-animate.min.js',
      'public/lib/angular-bootstrap/ui-bootstrap-tpls.min.js',
      'public/lib/angular-sanitize/angular-sanitize.min.js',
      'public/lib/ngstorage/ngStorage.min.js',
      'public/lib/ng-lodash/build/ng-lodash.min.js',
      'public/modules/common/imageByName/imageByName.js',
      'public/modules/common/imageByName/*.js',
      'public/modules/common/modal/modal.js',
      'public/modules/common/modal/*.js',
      'public/modules/common/modal/{loading,message}/*.js',
      'public/modules/logout/logout.js',
      'public/modules/logout/*.js',
      'public/modules/login/login.js',
      'public/modules/login/*.js',
      'public/modules/instances/instances.js',
      'public/modules/instances/*.js',
      'public/modules/instances/{create,list,get}/*.js',
      'public/modules/volumes/volumes.js',
      'public/modules/volumes/*.js',
      'public/modules/volumes/{create,list,attach}/*.js',
      'test/instances/{services,controllers,filters}/*.js'
    ],


    // list of files to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: false,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['Chrome'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: true,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity
  })
}
