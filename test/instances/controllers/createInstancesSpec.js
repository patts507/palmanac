describe("createInstancesController", function() {
  var $scope, $state, $q, instancesService;
  var mockData = {
    name: "server",
    flavor: {
      id: "30796715-1d57-406d-af87-8e7ee08ef89d",
      name: "m1.large",
      ram: 8192,
      disk: 0,
      vcpus: 4,
      swap: ""
    },
    image: {
      id: "e99be5a1-30ca-4122-83e0-e6729826df02",
      name: "docker",
      status: "ACTIVE",
      progress: 100,
      created: "2016-01-27T09:59:35Z",
      updated: "2016-01-27T09:59:52Z"
    }
  };

  beforeEach(function() {
    module("ui.router");
    module("modal");
    module("imageByName");
    module("instances");
    module("ngStorage");
    module("ngLodash");

    inject(function($rootScope, _instancesService_, _$state_, _$controller_, _modalService_, _imageByNameService_, _$q_, _lodash_, _$localStorage_) {
      instancesService = _instancesService_;
      $localStorage = _$localStorage_;
      modalService = _modalService_;
      imageByNameService = _imageByNameService_;
      $scope = $rootScope.$new();
      $q = _$q_;
      $state = _$state_;
      _ = _lodash_;

      $controller = _$controller_("createInstancesController", {
        $scope: $scope,
        instancesService: instancesService,
        $state: $state,
        lodash: _,
        imageByNameService: imageByNameService
      });
    });
  });

  it("should get information open stack", function() {
    var mockInformation = {
      flavors: [{
        id: "1a62e2ad-8020-4b4c-9db4-86e5f1136494",
        name: "single",
        ram: 1024,
        disk: 0,
        vcpus: 1,
        swap: ""
      }, {
        id: "3fa1a20e-82ae-40bf-8b88-a495d09cbb64",
        name: "small",
        ram: 512,
        disk: 0,
        vcpus: 1,
        swap: ""
      }],
      images: [{
        id: "768790a5-001a-47fb-a947-47f719b28130",
        name: "MongoDB3.2",
        status: "ACTIVE",
        progress: 100,
        created: "2016-02-25T06:53:39Z",
        updated: "2016-02-25T07:03:16Z"
      }, {
        id: "3fb59687-8f2c-4240-b1db-14220f66eab5",
        name: "RHEL6.6-Reduce",
        status: "ACTIVE",
        progress: 100,
        created: "2016-02-25T06:22:15Z",
        updated: "2016-02-25T06:29:01Z"
      }]
    };

    spyOn(instancesService, "getInformation").and.returnValues($q.resolve(mockInformation));

    $q.resolve($controller.getInformation())
      .then(function() {
        $scope.$apply();
        expect($scope.flavors).toEqual(mockInformation.flavors);
        expect($scope.images).toEqual(mockInformation.images);
      })
  });

  it("should create instance", function() {
    var mockInstanceCreate = {
      id: "7a3c3317-436a-451a-96f6-f3960617eda0",
      name: "test",
      status: "RUNNING",
      hostId: "8c8614c07bcf30bd153291609bf6af40c63c3e209fd7b531ee47e48c",
      addresses: {
        public: [],
        private: ["192.168.199.199", "10.252.200.11"]
      },
      imageId: "e99be5a1-30ca-4122-83e0-e6729826df02",
      flavorId: "30796715-1d57-406d-af87-8e7ee08ef89d",
      progress: 0,
      metadata: {},
      created: "2016-02-01T03:09:28Z",
      updated: "2016-02-01T03:09:34Z"
    };

    $scope.data = mockData;

    spyOn(instancesService, "createInstance").and.returnValues($q.resolve(mockInstanceCreate));
    spyOn($state, "go");

    $q.resolve($controller.createInstance())
      .then(function() {
        $scope.$apply();
        expect(instancesService.createInstance).toHaveBeenCalledWith({
          name: mockData.name,
          flavorId: mockData.flavor.id,
          imageId: mockData.image.id
        });
        expect($state.go).toHaveBeenCalledWith("showinstance", {
          instanceId: mockInstanceCreate.id
        });
      });
  });

});
