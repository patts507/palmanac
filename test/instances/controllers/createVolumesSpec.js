describe("createVolumeController", function() {
  var $scope, $state, $q, instancesService, volumesService, $stateParams;
  var mockData = {
    name: "pupuVolume",
    size: 1
  };

  beforeEach(function() {
    module("ui.router");
    module("modal");
    module("imageByName");
    module("volumes");
    module("instances");
    module("ngStorage");
    module("ngLodash");

    inject(function($rootScope, _instancesService_, _volumesService_, _$state_, _$controller_, _modalService_, _imageByNameService_, _$q_, _lodash_, _$localStorage_, _$window_, _$stateParams_) {
      instancesService = _instancesService_;
      volumesService = _volumesService_;
      $localStorage = _$localStorage_;
      modalService = _modalService_;
      imageByNameService = _imageByNameService_;
      $scope = $rootScope.$new();
      $q = _$q_;
      $stateParams = _$stateParams_;
      $window = _$window_;
      $state = _$state_;
      _ = _lodash_;

      $controller = _$controller_("createVolumesController", {
        $scope: $scope,
        instancesService: instancesService,
        volumesService: volumesService,
        $state: $state,
        $stateParams: $stateParams,
        lodash: _,
        imageByNameService: imageByNameService,
        $window: $window
      });
    });
  });

  it("should successfully create volume", function() {
    var mockInformation = {
      name: "pupuVolume",
      size: 1
    };

    spyOn(volumesService, "createVolumes").and.returnValues($q.resolve(mockInformation));
    spyOn(instancesService, "getInstances").and.returnValues($q.resolve(true));

    $scope.data = mockData;
    $scope.instances = [];

    $q.resolve($controller.createVolume())
      .then(function() {
        $scope.$apply();
        spyOn($state).toHaveBeenCalled();
      })
  });

  it("should successfully click instance when instance is unclicked", function() {
    $scope.instances = [{selected: false}, {selected: true}];
    index = 0;

    $q.resolve($controller.clickInstance(index))
      .then(function() {
        $scope.$apply();
        expect($scope.instances[index]).toEqual(true);
      })
  });


});
