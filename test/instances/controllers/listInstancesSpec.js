describe("listInstancesController", function() {
  var $scope, $state, $q, instancesService;
  var mockInstanceDatas = [{
    id: "3b9736ee-6a03-4a20-bada-c7765fbc6896",
    name: "serverStatus",
    status: "RUNNING",
    ip: "10.252.220.204",
    selected: false,
    flavor: {
      name: "small",
      ram: 512,
      disk: 0,
      vcpus: 1,
    },
    image: {
      name: "RHEL6.6-Reduce"
    }
  }, {
    id: "b043928b-f838-4051-bb6e-2df2e5e4f382",
    name: "server1",
    status: "RUNNING",
    ip: "10.252.220.204",
    selected: false,
    flavor: {
      name: "small",
      ram: 512,
      disk: 0,
      vcpus: 1
    },
    image: {
      name: "RHEL6.6-Reduce"
    }
  }];

  beforeEach(function() {
    module("ui.router");
    module("modal");
    module("instances");
    module("ngStorage");

    inject(function($rootScope, _instancesService_, _$state_, _$controller_, _modalService_, _$q_, _$localStorage_) {
      instancesService = _instancesService_;
      $scope = $rootScope.$new();
      $localStorage = _$localStorage_;
      $q = _$q_;
      $state = _$state_;

      $controller = _$controller_("listInstancesController", {
        $scope: $scope,
        $state: $state,
        instancesService: instancesService,
      });
    });
  });

  it("should get all instances", function() {
    spyOn(instancesService, "getInstances").and.returnValues($q.resolve(mockInstanceDatas));

    $q.resolve($controller.getInstances())
      .then(function() {
        $scope.$apply();
        expect($scope.instances).toEqual(mockInstanceDatas);
      })
  });

  it("should redirect to page showinstance", function() {
    var instanceId = "3468b2fd-01df-4711-9321-dbb1a0a64419";

    spyOn($state, "go");
    $controller.goToShow(instanceId);

    expect($state.go).toHaveBeenCalledWith("showinstance", {
      instanceId: instanceId
    });
  });
});
