describe("listVolumesController", function() {
  var $scope, $state, $q, instancesService, volumesService, $stateParams, attachVolumesService;
  var mockData = {
    name: "pupuVolume",
    size: 1
  };

  beforeEach(function() {
    module("ui.router");
    module("modal");
    module("volumes");
    module("instances");
    module("ngLodash");
    module("ngStorage");

    inject(function($rootScope, _volumesService_, _$state_, _$controller_, _modalService_, _$q_, _lodash_, _$window_, _$stateParams_, _attachVolumesService_) {
      volumesService = _volumesService_;
      modalService = _modalService_;
      attachVolumesService = _attachVolumesService_;
      $scope = $rootScope.$new();
      $q = _$q_;
      $stateParams = _$stateParams_;
      $window = _$window_;
      $state = _$state_;
      _ = _lodash_;

      $controller = _$controller_("listVolumesController", {
        $scope: $scope,
        volumesService: volumesService,
        $state: $state,
        $stateParams: $stateParams,
        lodash: _,
        $window: $window
      });
    });
  });

  it("should successfully get volume", function() {
    var mockInformation = {
      mocked: true
    };

    spyOn(volumesService, "getVolumes").and.returnValues(mockInformation);

    $scope.volumes = {};

    $q.resolve($controller.getVolumes())
      .then(function() {
        $scope.$apply();
        $scope.volumes = mockInformation;
      })
  });

  it("should successfully detachVolumes", function() {
    var mockInformation = {
      mocked: true
    };
    $scope.selected = 0;
    $scope.volumes = [{
      server: {id: '123'},
      volume: {id: '123'}
    }];

    spyOn(volumesService, "detachVolumes").and.returnValues(mockInformation);

    $q.resolve($controller.detachVolumes())
      .then(function() {
        spyOn($controller, "getVolume").toHaveBeenCalled(2);
      })
  });

  it("should successfully deleteVolumes", function() {
    var mockInformation = {
      mocked: true
    };
    $scope.selected = 0;
    $scope.volumes = [{
      volume: {id: '123'}
    }];

    spyOn(volumesService, "deleteVolumes").and.returnValues(mockInformation);

    $q.resolve($controller.deleteVolumes())
      .then(function() {
        spyOn($controller, "getVolume").toHaveBeenCalled(2);
      })
  });

  it("should successfully detachButton", function() {
    $scope.volumes = [{
      server: {name: 'yo'},
      volume: {name: 'yoo'}
    }]
    $q.resolve($controller.detachButton(0))
      .then(function() {
        spyOn(modalService, "showMessage").toHaveBeenCalled(1);
      })
  });

  it("should fail to detach if no server is attached to", function() {
    $scope.volumes = [{
      server: {}
    }]
    $q.resolve($controller.detachButton(0))
      .then(function() {
        spyOn(modalService, "showMessage").toHaveBeenCalled(1);
      })
  });

  it("should successfully click delete button", function() {
    var index = 0;
    $scope.volumes = [{
      volume: {name: ':('}
    }]
    $q.resolve($controller.deleteButton(index))
      .then(function() {
        $scope.apply();
        expect($scope.selected).toEqual(index);
        spyOn(modalService, "showMessage").toHaveBeenCalled(1);
      })
  });

  it("should successfully attach button", function() {
    var index = 0;
    $scope.volumes = [{
      volume: {name: ':('}
    }]
    var ob = spyOn(attachVolumesService, "showAttachModal").and.returnValues(true);
    $q.resolve($controller.attachButton(index))
      .then(function() {
        ob.toHaveBeenCalled(1);
      })
  });

});
