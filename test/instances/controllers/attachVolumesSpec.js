describe("attachVolumesController", function() {
  var $scope, $state, $q, instancesService, volumesService, $stateParams;
  data = {
    volume: {id: '123'},
  };

  beforeEach(function() {
    module("ui.router");
    module("modal");
    module("imageByName");
    module("volumes");
    module("instances");
    module("ngStorage");
    module("ngLodash");

    inject(function($rootScope, _instancesService_, _volumesService_, _$controller_, _modalService_, _$q_, _lodash_, _$window_) {
      instancesService = _instancesService_;
      volumesService = _volumesService_;
      modalService = _modalService_;
      $scope = $rootScope.$new();
      $window = _$window_;
      $q = _$q_;
      _ = _lodash_;

      $controller = _$controller_("attachVolumesController", {
        $scope: $scope,
        instancesService: instancesService,
        volumesService: volumesService,
        lodash: _,
        $window: $window,
        $uibModalInstance: {close: function(){}},
        data: data,
        fn: function() {}

      });
    });
  });

  it("should successfully create volume", function() {
    mock_instance = [{
      instance: {name: 'hi'}
    }];
    spyOn(instancesService, "getInstances").and.returnValues($q.resolve(mock_instance));

    $q.resolve($controller.getServers())
      .then(function() {
        $scope.$apply();
        expect($scope.instances).toEqual(mock_instance);
      })
  });

  it("should successfully select instance", function() {
    $scope.instances = [{selected: false}, {selected: false}];
    $scope.selected = undefined;
    index = 0;

    $q.resolve($controller.selectInstance(index))
      .then(function() {
        $scope.$apply();
        expect($scope.selected).toEqual(index);
        expect($scope.instances[index].selected).toEqual(true);
      })
  });

  it("should successfully click attach", function() {
    $scope.data = data;
    $scope.instances = [{id:'123', selected: false}];
    $scope.selected = 0;
    spyOn(volumesService, "attachVolumes").and.returnValues($q.resolve(true));
    $q.resolve($controller.selectInstance(index))
      .then(function() {
        $scope.$apply();
        spyOn(modal, "close").toHaveBeenCalled(1);
      })
  });



});
