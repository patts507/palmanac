describe("logoutSpec", function() {
  var $scope, $window, $localStorage, $controller;

  beforeEach(function() {
    module("ngStorage");
    module("logout");

    inject(function($rootScope, _$controller_, _$localStorage_, _$q_) {
      $localStorage = _$localStorage_;
      $scope = $rootScope.$new();
      $q = _$q_;
      $window = {
        location: { reload: function() {} }
      };

      $controller = _$controller_("logoutController", {
        $scope: $scope,
        $localStorage: _$localStorage_,
        $window: $window
      });
    });
  });

  it("should delete username and token from localstorage correctly", function() {
    $localStorage.username = 'test_username';
    $localStorage.token = 'test_token';

    $q.resolve($controller.logout())
      .then(function() {
        expect($localStorage.token).toEqual('');
        expect($localStorage.username).toEqual('');
      })
  });

});
