describe("getInstancesController", function() {
  var $scope, $state, $stateParams, $q, instancesService;
  var mockInstanceData = {
    server: {
      id: "7a3c3317-436a-451a-96f6-f3960617eda0",
      name: "test",
      status: "RUNNING",
      hostId: "8c8614c07bcf30bd153291609bf6af40c63c3e209fd7b531ee47e48c",
      addresses: {
        public: [],
        private: ["192.168.199.199", "10.252.200.11"]
      },
      imageId: "e99be5a1-30ca-4122-83e0-e6729826df02",
      flavorId: "30796715-1d57-406d-af87-8e7ee08ef89d",
      progress: 0,
      metadata: {},
      created: "2016-02-01T03:09:28Z",
      updated: "2016-02-01T03:09:34Z"
    },
    information: {
      flavors: {
        id: "30796715-1d57-406d-af87-8e7ee08ef89d",
        name: "m1.large",
        ram: 8192,
        disk: 0,
        vcpus: 4,
        swap: ""
      },
      images: {
        id: "e99be5a1-30ca-4122-83e0-e6729826df02",
        name: "docker",
        status: "ACTIVE",
        progress: 100,
        created: "2016-01-27T09:59:35Z",
        updated: "2016-01-27T09:59:52Z"
      }
    }
  };

  beforeEach(function() {
    module("ui.router");
    module("modal");
    module("instances");
    module("imageByName");
    module("ngLodash");
    module("ngStorage");

    inject(function(_$state_, $rootScope, _instancesService_, _$controller_, _$q_, _modalService_, _imageByNameService_, _$stateParams_, _$localStorage_) {
      instancesService = _instancesService_;
      $scope = $rootScope.$new();
      $q = _$q_;
      $state = _$state_;
      $stateParams = _$stateParams_;
      $localStorage = _$localStorage_;
      imageByNameService = _imageByNameService_;

      $controller = _$controller_("getInstancesController", {
        $scope: $scope,
        $state: $state,
        $stateParams: $stateParams,
        instancesService: instancesService,
        imageByNameService: imageByNameService
      });
    });
  });

  it("should get instance information", function() {
    $stateParams.instanceId = "3468b2fd-01df-4711-9321-dbb1a0a64419";

    spyOn(instancesService, "getInstance").and.returnValues($q.resolve(mockInstanceData));

    $q.resolve($controller.getInstance())
      .then(function() {
        $scope.$apply();
        expect($scope.id).toEqual($stateParams.instanceId);
        expect(instancesService.getInstance).toHaveBeenCalledWith($scope.id);
        expect($scope.server).toEqual(mockInstanceData.server);
        expect($scope.information).toEqual(mockInstanceData.information);
      });
  });

  it("should start instance", function() {
    $scope.id = "3468b2fd-01df-4711-9321-dbb1a0a64419";

    spyOn(instancesService, "startInstance").and.returnValues($q.resolve(mockInstanceData));

    $q.resolve($controller.startInstance())
      .then(function() {
        $scope.$apply();

        expect(instancesService.startInstance).toHaveBeenCalledWith($scope.id);
        expect($scope.server).toEqual(mockInstanceData.server);
        expect($scope.information).toEqual(mockInstanceData.information);
      });
  });

  it("should stop instance", function() {
    $scope.id = "3468b2fd-01df-4711-9321-dbb1a0a64419";

    spyOn(instancesService, "stopInstance").and.returnValues($q.resolve(mockInstanceData));

    $q.resolve($controller.stopInstance())
      .then(function() {
        $scope.$apply();
        expect(instancesService.stopInstance).toHaveBeenCalledWith($scope.id);
        expect($scope.server).toEqual(mockInstanceData.server);
        expect($scope.information).toEqual(mockInstanceData.information);
      });
  });

  it("should reboot instance", function() {
    $scope.id = "3468b2fd-01df-4711-9321-dbb1a0a64419";

    spyOn(instancesService, "rebootInstance").and.returnValues($q.resolve(mockInstanceData));

    $q.resolve($controller.rebootInstance())
      .then(function() {
        $scope.$apply();

        expect(instancesService.rebootInstance).toHaveBeenCalledWith($scope.id);
        expect($scope.server).toEqual(mockInstanceData.server);
        expect($scope.information).toEqual(mockInstanceData.information);
      })
  });

  it("should delete instance", function() {
    $scope.id = "3468b2fd-01df-4711-9321-dbb1a0a64419";

    spyOn(instancesService, "deleteInstance").and.returnValues($q.resolve(mockInstanceData));
    spyOn($state, "go");

    $q.resolve($controller.deleteInstance())
      .then(function() {
        $scope.$apply();

        expect(instancesService.deleteInstance).toHaveBeenCalledWith($scope.id);
        expect($state.go).toHaveBeenCalledWith("listinstance");
      });
  });
});
