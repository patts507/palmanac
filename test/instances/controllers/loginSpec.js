describe("loginSpec", function() {
  var $scope, $window, $localStorage, $controller;

  beforeEach(function() {
    module("ngStorage");
    module("login");
    module("modal");
    module("ui.bootstrap");

    inject(function($rootScope, _$http_, _$uibModal_, _$controller_, _$localStorage_, _$q_, _loginService_, _modalService_) {
      $localStorage = _$localStorage_;
      $scope = $rootScope.$new();
      $http = _$http_;
      $loginService = _loginService_;
      $modalService= _modalService_;
      $q = _$q_;
      $window = {
        location: { reload: function() {} }
      };
      var modal = _$uibModal_.open({
        template: '<div></div>',
      });
      $controller = _$controller_("loginModalController", {
        $http: $http,
        $scope: $scope,
        $uibModalInstance: modal,
        $localStorage: $localStorage,
        $loginService: $loginService,
        $modalService: $modalService,
        $window: $window
      });
    });
  });

  it("should login successfully", function() {
    $scope.username = 'pp';
    $scope.password = '1234567890';
    $localStorage.token = '';
    $localStorage.username = '';

    $q.resolve($controller.loginButton())
      .then(function() {
        expect($localStorage.token).not.toEqual('');
        expect($localStorage.username).not.toEqual('');
      })
  });

});
