describe("Modal Controller", function() {

  var $scope;

  beforeEach(function() {
    module('modal');
    module('login');

    inject(function($rootScope, _$controller_, _modalService_, $uibModal, _loginService_) {
      $scope = $rootScope.$new();
      loginService = _loginService_;
      modalService = _modalService_;
      modal = $uibModal.open({
        template: '<div></div>',
      });
      $controller = _$controller_("modalController", {
        $scope: $scope,
        $uibModalInstance: modal,
        data: {
          title: 'Error',
          description: ['hooray'],
          button: [{ action: function() {return null;} }]
        }
      });
    });
  });

  it("should update scope correctly after calling controller", function() {
    spyOn(modal, 'close');
    $controller.click(0);
    expect(modal.close).toHaveBeenCalled();
    expect($controller.data.title).toEqual('Error');
    expect($controller.data.description).toEqual(['hooray']);
  });
});
