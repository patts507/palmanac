describe("Instance Filter", function() {
  var filter;

  beforeEach(function() {
    module('instances');

    inject(function($filter) {
      filter = $filter;
    });
  });

  describe('memorySize', function() {
    it("should return ram value concat string with MB", function() {
      var ramFilter = filter('memorySize', {});
      expect(ramFilter(128)).toBe('128 MB');
      expect(ramFilter("512")).toBe('512 MB');
    });
  });

  describe('storageSize', function() {
    it("should return disk value concat string with GB", function() {
      var diskFilter = filter('storageSize', {});
      expect(diskFilter(20)).toBe('20 GB');
      expect(diskFilter("40")).toBe('40 GB');
    });
  });
});
