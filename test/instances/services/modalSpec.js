describe("Modal Service", function() {

  beforeEach(function() {
    module('modal');
    inject(function(_modalService_, $uibModal) {
      modalService = _modalService_;
      modal = $uibModal;
    });
  });


  it("should call open modal", function() {
    spyOn(modal, "open").and.returnValues({result: true});
    modalService.showMessage('Error', ['hooray']);
    expect(modal.open).toHaveBeenCalled();
  });
});
