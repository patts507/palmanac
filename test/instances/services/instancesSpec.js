describe("Instance Service", function() {
  var instancesService, $http, $q, id, instanceData;

  beforeEach(function() {
    module("instances");
    module("ngStorage");

    id = "b52e70f4-d401-422e-a18c-fcb7f46a9c60";
    instanceData = {
      server: {
        id: "b043928b-f838-4051-bb6e-2df2e5e4f382",
        name: "server1",
        status: "RUNNING",
        hostId: "8c8614c07bcf30bd153291609bf6af40c63c3e209fd7b531ee47e48c",
        addresses: {
          public: [],
          private: ["192.168.199.190", "10.252.200.11"]
        },
        imageId: "3fb59687-8f2c-4240-b1db-14220f66eab5",
        flavorId: "3fa1a20e-82ae-40bf-8b88-a495d09cbb64",
        progress: 0,
        metadata: {},
        created: "2016-02-26T04:09:54Z",
        updated: "2016-02-26T04:10:00Z"
      },
      information: {
        flavor: {
          id: "3fa1a20e-82ae-40bf-8b88-a495d09cbb64",
          name: "small",
          ram: 512,
          disk: 0,
          vcpus: 1,
          swap: ""
        },
        image: {
          id: "3fb59687-8f2c-4240-b1db-14220f66eab5",
          name: "RHEL6.6-Reduce",
          status: "ACTIVE",
          progress: 100,
          created: "2016-02-25T06:22:15Z",
          updated: "2016-02-25T06:29:01Z"
        }
      }
    };

    inject(function(_instancesService_, _$http_, _$q_, _$localStorage_) {
      instancesService = _instancesService_;
      $localStorage = _$localStorage_;
      $http = _$http_;
      $q = _$q_;
    });
  });

  it("should return information of open stack", function() {
    var information = {
      flavors: [{
        id: "1a62e2ad-8020-4b4c-9db4-86e5f1136494",
        name: "single",
        ram: 1024,
        disk: 0,
        vcpus: 1,
        swap: ""
      }, {
        id: "3fa1a20e-82ae-40bf-8b88-a495d09cbb64",
        name: "small",
        ram: 512,
        disk: 0,
        vcpus: 1,
        swap: ""
      }],
      images: [{
        id: "768790a5-001a-47fb-a947-47f719b28130",
        name: "MongoDB3.2",
        status: "ACTIVE",
        progress: 100,
        created: "2016-02-25T06:53:39Z",
        updated: "2016-02-25T07:03:16Z"
      }, {
        id: "3fb59687-8f2c-4240-b1db-14220f66eab5",
        name: "RHEL6.6-Reduce",
        status: "ACTIVE",
        progress: 100,
        created: "2016-02-25T06:22:15Z",
        updated: "2016-02-25T06:29:01Z"
      }]
    };

    spyOn($http, "get").and.returnValues($q.resolve(information));

    instancesService.getInformation();

    expect($http.get).toHaveBeenCalledWith("/instance/information");
  });

  it("should send instance data for create", function() {
    var data = {
      name: "bas",
      image: "b52e70f4-d401-422e-a18c-fcb7f46a9c60",
      flavor: "30796715-1d57-406d-af87-8e7ee08ef89d"
    };

    spyOn($http, "post").and.returnValues($q.resolve(id));

    instancesService.createInstance(data);

    expect($http.post).toHaveBeenCalledWith("/instance", data);
  });

  it("should get all instances", function() {
    var instanceDatas = [{
      server: {
        id: "3b9736ee-6a03-4a20-bada-c7765fbc6896",
        name: "serverStatus",
        status: "RUNNING",
        hostId: "8c8614c07bcf30bd153291609bf6af40c63c3e209fd7b531ee47e48c",
        addresses: {
          public: [],
          private: ["192.168.199.191"]
        },
        imageId: "3fb59687-8f2c-4240-b1db-14220f66eab5",
        flavorId: "3fa1a20e-82ae-40bf-8b88-a495d09cbb64",
        progress: 0,
        metadata: {},
        created: "2016-02-26T04:09:54Z",
        updated: "2016-02-26T04:52:08Z"
      },
      information: {
        flavor: {
          id: "3fa1a20e-82ae-40bf-8b88-a495d09cbb64",
          name: "small",
          ram: 512,
          disk: 0,
          vcpus: 1,
          swap: ""
        },
        image: {
          id: "3fb59687-8f2c-4240-b1db-14220f66eab5",
          name: "RHEL6.6-Reduce",
          status: "ACTIVE",
          progress: 100,
          created: "2016-02-25T06:22:15Z",
          updated: "2016-02-25T06:29:01Z"
        }
      }
    }, {
      server: {
        id: "b043928b-f838-4051-bb6e-2df2e5e4f382",
        name: "server1",
        status: "RUNNING",
        hostId: "8c8614c07bcf30bd153291609bf6af40c63c3e209fd7b531ee47e48c",
        addresses: {
          public: [],
          private: ["192.168.199.190"]
        },
        imageId: "3fb59687-8f2c-4240-b1db-14220f66eab5",
        flavorId: "3fa1a20e-82ae-40bf-8b88-a495d09cbb64",
        progress: 0,
        metadata: {},
        created: "2016-02-26T04:09:54Z",
        updated: "2016-02-26T04:10:00Z"
      },
      information: {
        flavor: {
          id: "3fa1a20e-82ae-40bf-8b88-a495d09cbb64",
          name: "small",
          ram: 512,
          disk: 0,
          vcpus: 1,
          swap: ""
        },
        image: {
          id: "3fb59687-8f2c-4240-b1db-14220f66eab5",
          name: "RHEL6.6-Reduce",
          status: "ACTIVE",
          progress: 100,
          created: "2016-02-25T06:22:15Z",
          updated: "2016-02-25T06:29:01Z"
        }
      }
    }];

    spyOn($http, "get").and.returnValues($q.resolve(instanceDatas));

    instancesService.getInstances();

    expect($http.get).toHaveBeenCalledWith("/instance");
  });

  it("should get instance data wiht instance id", function() {
    spyOn($http, "get").and.returnValues($q.resolve(instanceData));

    instancesService.getInstance(id);

    expect($http.get).toHaveBeenCalledWith("/instance/" + id);
  });

  it("should delete instance", function() {
    spyOn($http, "delete").and.returnValues($q.resolve(instanceData));

    instancesService.deleteInstance(id);

    expect($http.delete).toHaveBeenCalledWith("/instance", {
      params: {
        instances: id
      }
    });
  });

  it("should start instance wiht instance id", function() {
    spyOn($http, "put").and.returnValues($q.resolve(instanceData));

    instancesService.startInstance(id);

    expect($http.put).toHaveBeenCalledWith("/instance/" + id + "/start");
  });

  it("should stop instance wiht instance id", function() {
    spyOn($http, "put").and.returnValues($q.resolve(instanceData));

    instancesService.stopInstance(id);

    expect($http.put).toHaveBeenCalledWith("/instance/" + id + "/stop");
  });

  it("should reboot instance wiht instance id", function() {
    spyOn($http, "put").and.returnValues($q.resolve(instanceData));

    instancesService.rebootInstance(id);

    expect($http.put).toHaveBeenCalledWith("/instance/" + id + "/reboot");
  });
});
