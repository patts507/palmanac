'use strict'
angular.module('Main', ['ui.router',
                        'core',
                        'modal',
                        'palmanacs',
                        'ui.bootstrap',
                        'ngStorage',
                        'ngLodash',
                        'ngCsv',]);
