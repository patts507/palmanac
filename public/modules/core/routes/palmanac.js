angular.module('palmanacs').config(['$stateProvider', function($stateProvider) {

  $stateProvider
    .state('index', {
      url: '/',
      templateUrl: "/modules/palmanac/calculator/calculator-view.html"
    });

}]);
