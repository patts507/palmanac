angular.module("modal").controller("modalController", ["$scope", "$uibModalInstance", "data", function ($scope, modal, data, action) {
  var self = this;
  this.scope = $scope;

  self.data = data;

  this.click = function (index) {
    modal.close("cancel");
    //self.data.button[index].action(data.button[index].arg);
    try {
      var regex = new RegExp(/Unauthorized/g);
      if(regex.test(self.data.desc[0])) {
        loginService.showLoginModal();
        $state.go('index');
      }
    }
    catch(err){}
  };

}]);
