angular.module('modal').controller('loadingModalController', ['$scope', '$uibModalInstance', 'task', function ($scope, modal, task) {
  var self = this;
  this.scope = $scope;

  self.task = task;

  self.executeTask = function () {
    self.task.task(self.task.data)
      .then(self.taskSuccess, self.taskFail)
  };

  self.taskSuccess = function (res) {
    self.task.success(res);
    self.closeModal('close', 'success');
  };

  self.taskFail = function (res) {
    self.task.fail(res, function () { });
    self.closeModal('close');
  };

  self.closeModal = function (action, result) {
    modal[action](result);
  }

}]);

