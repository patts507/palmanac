angular.module("modal").service("modalService", ["$uibModal", function (modal) {

  var data = {};

  this.showMessage = function (title, description, button) {
    var data = {
      title: title,
      desc: description,
      button: button || [{
        label: 'Ok :(',
        action: function(){},
        type: 'primary'
      }]
    };
    var modalInstance = modal.open({
      animation: true,
      templateUrl: "/modules/common/modal/message/message-view.html",
      controller: "modalController",
      controllerAs: "modalCtrl",
      keyboard: false,
      backdrop: "static",
      resolve: {
        data: function () {
          return data;
        }
      }
    });
    return modalInstance.result;
  };

  this.showLoadingSpinner = function (task) {
    var modalInstance = modal.open({
      animation: true,
      templateUrl: "/modules/common/modal/loading/loading-view.html",
      controller: 'loadingModalController',
      controllerAs: 'loadingCtrl',
      keyboard: false,
      backdrop: 'static',
      size: 'md',
      resolve: {
        task: function () {
          return task;
        }
      }
    });
    return modalInstance.result;
  };

  /*this.renameInstance = function (instanceId,oldName) {
     var data = {
       instanceId: instanceId,
       oldName: oldName,
     };

     var modalInstance = modal.open({
      animation: true,
      templateUrl: "/modules/instances/rename/inputmessage-view.html",
      controller: "inputModalController",
      controllerAs: "inputModalCtrl",
      keyboard: false,
      backdrop: "static",
      resolve: {
        data: function () {
          return data;
        }
      }
    });
    return modalInstance.result;
  };

  this.renameVolume = function (volume) {
     var modalInstance = modal.open({
      animation: true,
      templateUrl: "/modules/volumes/rename/renamevolume-view.html",
      controller: "renameVolumeController",
      controllerAs: "renameVolumeCtrl",
      keyboard: false,
      backdrop: "static",
      resolve: {
        data: function () {
          return volume;
        }
      }
    });
    return modalInstance.result;
  };*/

}]);
