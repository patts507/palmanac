'use strict'
angular.module("palmanacs").controller("calculatorController", ["$scope","$state", "modalService", "palmanacService",function ($scope, $state, modalService, palmanacService, action) {

  var vm = $scope;

  vm.message = 'Palmanac - Tool';
  vm.appname = undefined;
  vm.result1 = undefined;
  vm.result2 = undefined;
  vm.result3 = undefined;
  vm.result4 = undefined;
  vm.result5 = undefined;
  vm.result6 = undefined;
  $(":file").filestyle();

  var concorenumber = 4,
    conmessagesize = 0.00,
    conlog = 0.00,
    conhttp = 0.00,
    condiameter = 0.00,
    conldap = 0.00,
    consmpp = 0.00,
    contcp = 0,
    conother = 0,
    conweight = 0,
    conconcurrent = 0,
    conreqtran = 1,
    _result = undefined;

  vm.palmanacs = {
    corenumber: function num(val) {
      return arguments.length ? vm.corenumber = (concorenumber = parseInt(val)) : vm.corenumber = concorenumber;
    }
    , messagesize: function messagesize(val) {
      return arguments.length ? vm.messagesize = (conmessagesize = parseFloat(val)) : vm.messagesize = conmessagesize;
    }
    , log: function log(val) {
      return arguments.length ? vm.log = (conlog = parseFloat(val)) : vm.log = conlog;
    }
    , http: function http(val) {
      return arguments.length ? vm.protocolhttp = (conhttp = parseFloat(val)) : vm.protocolhttp = conhttp;
    }
    , diameter: function diameter(val) {
      return arguments.length ? vm.protocoldiameter = (condiameter = parseFloat(val)) : vm.protocoldiameter = condiameter;
    }
    , ldap: function ldap(val) {
      return arguments.length ? vm.protocolldap = (conldap = parseFloat(val)) : vm.protocolldap = conldap;
    }
    , smpp: function smpp(val) {
      return arguments.length ? vm.protocolsmpp = (consmpp = parseFloat(val)) : vm.protocolsmpp = consmpp;
    }
    , tcp: function tcp(val) {
      return arguments.length ? vm.protocoltcp = (contcp = parseFloat(val)) : vm.protocoltcp = contcp;
    }
    , other: function other(val) {
      return arguments.length ? vm.protocolother = (conother = parseFloat(val)) : vm.protocolother = conother;
    }
    , weight: function weight(val) {
      return arguments.length ? vm.weight = (conweight = parseFloat(val)) : vm.weight = conweight;
    }
    , concurrent: function concurrent(val) {
      return arguments.length ? vm.concurrent = (conconcurrent = parseInt(val)) : vm.concurrent = conconcurrent;
    }
    , reqtran: function reqtran(val) {
      return arguments.length ? vm.reqtran = (conreqtran = parseFloat(val)) : vm.reqtran = conreqtran;
    }
  };

  this.getInformation = function(){
    modalService.showLoadingSpinner({
      task: palmanacService.getInformation,
      success: function(result){
        _result = result.data
        vm.nameExport = _result.nameExport;
        vm.ecs = _result.ecs;
        vm.ec = _.head(vm.ecs);
        vm.eqxversions = _result.eqxVersions;
        vm.eqxversion = _.head(vm.eqxversions);
        vm.typeofmachs = _result.typeofmachs;
        vm.typeofmach = _.head(vm.typeofmachs);
        vm.minsizeKB = _result.minsizeKB;
        vm.maxsizeKB = _result.maxsizeKB;
        vm.minProtocol = _result.minProtocol;
        vm.maxProtocol = _result.maxProtocol;
        vm.minWeight = _result.minWeight;
        vm.maxWeight = _result.maxWeight;
        vm.minConcurrent = _result.minConcurrent;
        vm.maxConcurrent = _result.maxConcurrent
        vm.minReqtran = _result.minReqtran;
        vm.maxReqtran = _result.maxReqtran;
        vm.minCornum = _result.minCornum;
        vm.minCornumRange = _result.minCornumRange;
        vm.maxCornum = _result.maxCornum;
      },
      fail: function(err){
        modalService.showMessage('Error', err.errors);
      }
    });
  };

  this.calculator = function () {
    var sum = parseFloat(vm.protocolhttp) + parseFloat(vm.protocoldiameter) + parseFloat(vm.protocolldap) + parseFloat(vm.protocolsmpp) + parseFloat(vm.protocoltcp) + parseFloat(vm.protocolother);
    modalService.showLoadingSpinner({
      data: {appname: vm.appname,
            eqxversion: vm.eqxversion,
            ec: vm.ec,
            log: vm.log,
            msgsize: vm.messagesize,
            http: vm.protocolhttp,
            diameter: vm.protocoldiameter,
            ldap: vm.protocolldap,
            smpp: vm.protocolsmpp,
            tcp: vm.protocoltcp,
            other: vm.protocolother,
            weight: vm.weight,
            concurrent: vm.concurrent,
            reqtran: vm.reqtran,
            cornumber: vm.corenumber,
            typeofmach: vm.typeofmach,
            Protocol : sum
      },
      task: palmanacService.calculator,
      success: function(result){
        console.log(result.data);
        vm.result5 = result.data.tps;
        vm.result6 = result.data.rps;
      },
      fail: function(err){
        modalService.showMessage('Error', err.errors);
      }
    });
  };

  vm.weightShow = function () {
    if (vm.protocolother > 0) {
      vm.weightstatus = "true";
    }
    else {
      vm.weightstatus = "false";
    }
  };

  this.clear = function () {
    vm.appname = '';
    vm.ec = _.head(_result.ecs);
    vm.eqxversion = _.head(_result.eqxVersions);
    vm.typeofmach = _.head(_result.typeofmachs);
    vm.palmanacs.corenumber(4);
    vm.palmanacs.messagesize(0);
    vm.palmanacs.weight(0);
    vm.palmanacs.log(0);
    vm.palmanacs.other(0);
    vm.palmanacs.concurrent(0);
    vm.palmanacs.reqtran(1);
    vm.palmanacs.other(0);
    vm.palmanacs.tcp(0);
    vm.palmanacs.ldap(0);
    vm.palmanacs.smpp(0);
    vm.palmanacs.diameter(0);
    vm.palmanacs.http(0);
    $(":file").filestyle('clear');
    vm.result5 = undefined;
    vm.result6 = undefined;
  };

  $(document).ready(function () {
    $('[data-toggle="popover"]').popover();
  });

  this.getExportData = function(){
    return [{type:"Application_Name", value:vm.appname},
            {type:"EQUINOX_Version", value:vm.eqxversion},
            {type:"Type_of_EC", value:vm.ec},
            {type:"Message_size_KB", value:vm.palmanacs.messagesize()},
            {type:"Log_size_KB", value: vm.palmanacs.log()},
            {type:"Protocol_HTTP", value:vm.palmanacs.http()},
            {type:"Protocol_Diameter", value:vm.palmanacs.diameter()},
            {type:"Protocol_LDAP", value:vm.palmanacs.ldap()},
            {type:"Protocol_SMPP", value:vm.palmanacs.smpp()},
            {type:"Protocol_TCP", value:vm.palmanacs.tcp()},
            {type:"Other_Protocol", value:vm.palmanacs.other()},
            {type:"Weight", value:vm.palmanacs.weight()},
            {type:"Concurrent", value:vm.palmanacs.concurrent()},
            {type:"Transaction_Per_Request", value:vm.palmanacs.reqtran()},
            {type:"Core_Number", value:vm.palmanacs.corenumber()},
            {type:"Type_of_machine", value:vm.typeofmach},
            {type:"TPS", value:vm.result5},
            {type:"RPS", value:vm.result6}
            ];
  };

  this.showContent = function($fileContent){
    var Value = _.chain($fileContent)
                 .split("\r\n")
                 .split(",")
                 .compact()
                 .chunk(2)
                 .fromPairs()
                 .value();
    console.log(Value);
    vm.appname = Value.Application_Name;
    vm.ec = Value.Type_of_EC;
    vm.palmanacs.corenumber(Value.Core_Number);
    vm.palmanacs.messagesize(Value.Message_size_KB);
    vm.eqxversion = Value.EQUINOX_Version;
    vm.typeofmach = Value.Type_of_machine;
    vm.palmanacs.log(Value.Log_size_KB);
    vm.palmanacs.other(Value.Other_Protocol);
    vm.palmanacs.tcp(Value.Protocol_TCP);
    vm.palmanacs.ldap(Value.Protocol_LDAP);
    vm.palmanacs.smpp(Value.Protocol_SMPP);
    vm.palmanacs.diameter(Value.Protocol_Diameter);
    vm.palmanacs.http(Value.Protocol_HTTP);
    vm.palmanacs.weight(Value.Weight);
    vm.palmanacs.concurrent(Value.Concurrent);
    vm.palmanacs.reqtran(Value.Transaction_Per_Request);
    this.calculator();
  };


}]);
