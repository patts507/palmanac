angular.module('palmanacs').service('palmanacService', ['lodash', '$localStorage', '$http', '$q', function(_, $localStorage, $http, $q) {

  this.getInformation = function() {
    return $http.get('/information')
      .then(function(result) {
        return result;
      })
      .catch(function(err) {
        return $q.reject(err.data);
      });
  };

  this.calculator = function(data){
    return $http.post('/calculator', data)
      .then(function(result){
        return result;
      })
      .catch(function(err){
        return $q.reject(err.data);
      });
  };


}]);
